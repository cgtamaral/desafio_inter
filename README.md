# Desafio Técnico Java - Processo seletivo Banco Inter

Este projeto permite realizar:

- criação, consulta, edição e exclusão(lógica) de um usuário;
- Associar calculo de um dígito único de um inteiro para um usuário existente;
- Cálcular o resultado de um dígito único de um inteiro;
- Consultar os resultados de digitos únicos associados a um usuário;
- Enviar chave pública para um usuário existente;
- Reativar um usuário;

### Pré-requisitos

- Eclipse Photon ou superior;
- Java JDK 11 ou superior;
- Apache Maven versão 3.6.0 ou superior;

### Instalação e execução da aplicação em ambiente local

- Clonar o projeto em um diretório desejado;
- Importar o código fonte no Eclipse, selecionar a opção Existing Maven Projects;
- No Eclipse, acessar a aba Project Explorer, clicar no projeto com o botão direito do mouse e selecionar a opção run ou debug, Java Aplicaton e selecionar o arquivo DesafioApplication.java;

### Execução dos testes unitários em ambiente local

- Pré requisito: Ter importado o projeto no eclipse conforme passo anterior;
- No Eclipse, acessar a aba Project Explorer, clicar no projeto com o botão direito do mouse e selecionar a opção run ou debug, JUnit Test;

### Formato de chave pública para o usuário

- A chave pública deve ser gerada utilizando o algoritmo RSA de tamanho 2048 e ter o formato de saida em Base64.
- O processo de gerar as chaves privadas e públicas pode ser realizado a fim de testes no site:
```sh
https://www.devglan.com/online-tools/rsa-encryption-decryption
```
- Observação: A API não possui validação de formato da chave pública. Se a API receber um valor inválido para a chave pública os dados
de nome e email do usuário serão apresentados com erro.

### Documentação da API

* A documentação da API gerada pelo Swagger está disponivel após start da aplicação no endereço:

```sh
http://localhost:8081/desafio/swagger-ui.html
```