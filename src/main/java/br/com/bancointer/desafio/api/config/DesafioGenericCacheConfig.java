package br.com.bancointer.desafio.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.bancointer.desafio.api.util.cache.DesafioGenericCache;
import br.com.bancointer.desafio.api.util.cache.DesafioGenericCacheImpl;

@Configuration
public class DesafioGenericCacheConfig {

    @Bean
    public <K, V> DesafioGenericCache<K, V> getCache(@Value("${app.cache.timeout}") Long cacheTimeout) {
        return new DesafioGenericCacheImpl<K, V>(cacheTimeout);
    }
}