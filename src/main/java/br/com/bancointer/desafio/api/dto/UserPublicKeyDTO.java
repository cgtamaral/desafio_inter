package br.com.bancointer.desafio.api.dto;

public class UserPublicKeyDTO
{
	private String publicKey;
	
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
}
