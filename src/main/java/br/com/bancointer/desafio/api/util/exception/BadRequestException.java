package br.com.bancointer.desafio.api.util.exception;

public class BadRequestException extends Exception 
{ 
    /**
	 * 
	 */
	private static final long serialVersionUID = 8365078291366642370L;

	public BadRequestException(String errorMessage) 
    {
        super(errorMessage);
    }
}
