package br.com.bancointer.desafio.api.controller.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.dto.ResponseDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitRequestDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.service.UniqueDigitService;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;

@RestController
@RequestMapping("/v1/public")
@CrossOrigin(origins = "*")
@Api(value = "uniqueDigits", description = "Recurso para execução de cálculos de digito único. Permite executar cálculos de digito único e associar o cálculo em tempo de execução a um usuário caso o mesmo seja informado na requisição.", tags={ "uniqueDigits"})
public class UniqueDigitController {

	private static final Logger log = LoggerFactory.getLogger(UniqueDigitController.class);
	
	private UniqueDigitService uniqueDigitService;
	
	public UniqueDigitController(UniqueDigitService p_uniqueDigitService) 
	{
		this.uniqueDigitService = p_uniqueDigitService;
	}
	

	@ApiOperation(value = "Executa o cálculo de digito único para os parâmetros informados e associa o resultado a um usuário quando o mesmo foi informado.", nickname = "calculateUniqueDigits", notes = "", tags={ "uniqueDigits"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Resultados de digitos únicos calculados com sucesso! | Resultados de digitos únicos calculados e associados ao usuário com sucesso!"),
    					   @ApiResponse(code = 400, message = "O usuário informado não foi encontrado e o cáculo nao foi realizado | O valor informado para o campo n é maior que 10ˆ1000000. | O valor informado para o campo n é menor que 1. | O valor informado para o campo n é invalido. | O valor informado para o campo k é maior que 10ˆ5. | O valor informado para o campo k é menor que 1. | O campo n não foi informado."),
    					   @ApiResponse(code = 404, message = "O usuário informado não foi encontrado.")})
	@PostMapping(value="/uniqueDigits")
	public ResponseEntity<ResponseDTO<UniqueDigitResponseDTO>> calculateUniqueDigits(@ApiParam(value = "Dados cálculo de digito único. O campo nValue é obrigatório.", required = true) @RequestBody UniqueDigitRequestDTO uniqueDigitsRequestDTO)
	{
		log.info("Calculando digito unico...");
		
		ResponseDTO<UniqueDigitResponseDTO> response = new ResponseDTO<UniqueDigitResponseDTO>();
		UniqueDigitResponseDTO uniqueDigitResponseDTO = null;
  		try 
  		{
  			uniqueDigitResponseDTO = uniqueDigitService.calculateUniqueDigits(uniqueDigitsRequestDTO);
		} 
  		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
  		catch (BadRequestException e) 
  		{
			response.setStatus(HttpStatus.BAD_REQUEST.toString());
			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.badRequest().body(response);
		}
  		
  		String returnMessage = uniqueDigitResponseDTO.getUserId() != null ? "Resultados de digitos únicos calculados e associados ao usuário com sucesso!" : "Resultados de digitos únicos calculados com sucesso!";
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(uniqueDigitResponseDTO);
		response.setMessages(Lists.newArrayList(returnMessage));
		
  		return ResponseEntity.ok(response);
	}
}
