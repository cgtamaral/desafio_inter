package br.com.bancointer.desafio.api.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.dto.UserPublicKeyDTO;
import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.entity.UniqueDigit;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.mapper.UniqueDigitMapper;
import br.com.bancointer.desafio.api.mapper.UserMapper;
import br.com.bancointer.desafio.api.repository.UserRepository;
import br.com.bancointer.desafio.api.service.UserService;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

@Service
public class UserServiceImpl implements UserService
{
	private UserRepository userRepository;
	
	public UserServiceImpl(UserRepository p_userRepository) {
		this.userRepository = p_userRepository;
	}
	
	@Override
	public Optional<User> getUserById(Long userId) 
	{
		return userRepository.findById(userId);
	}
	
	@Override
	public UserResponseDTO getUserByEmail(String email) throws NotFoundException, BadRequestException
	{
		UserResponseDTO userResponseDTO = null;
		if(email!=null && !email.isEmpty())
		{
			boolean isValidEmail = isValid(email);
			if(isValidEmail)
			{	
				User user = userRepository.findByEmail(email);
				
				if(user!=null)
				{
					userResponseDTO = UserMapper.mapUserToUserResponseDTO(user);	
				}
				else
				{
					throw new NotFoundException("Nenhum usuário foi encontrado para o email informado.");
				}
			}
			else
			{
				throw new BadRequestException("O valor informado para o parâmentro email é inválido.");		
			}
		}
		else
		{
			throw new BadRequestException("O parâmentro email não foi informado.");		
		}
		
		return userResponseDTO;
	}
	
	@Override
	public List<UniqueDigitResponseDTO> getUniqueDigitsByUser(Long userId) throws NotFoundException
	{
		Optional<User> user = userRepository.findById(userId);
		List<UniqueDigitResponseDTO> uniqueDigitResponseDTOList = null;
		if(user.isPresent())
		{
			List<UniqueDigit> uniqueDigitList = user.get().getUniqueDigitsList();
			if(uniqueDigitList!=null && uniqueDigitList.size() > 0)
			{
				uniqueDigitResponseDTOList = UniqueDigitMapper.mapUniqueDigitListToUniqueDigitResponseDTOList(uniqueDigitList);		
			}
			else
			{
				throw new NotFoundException("Nenhum resultado de digito único para o usuário informado foi encontrado.");		
			}
		}
		else
		{
			throw new NotFoundException("O usuário informado não foi encontrado.");						
		}
		
		return uniqueDigitResponseDTOList;
	}


	@Override
	public UserResponseDTO createUser(UserRequestDTO userRequestDTO) throws BadRequestException
	{
		UserResponseDTO userResponseDTO = null;

		isValidInputs(userRequestDTO.getName(),userRequestDTO.getEmail());
		boolean isValidEmail = isValid(userRequestDTO.getEmail());
		if(isValidEmail)
		{
			User user = userRepository.findByEmail(userRequestDTO.getEmail());
			if(user!=null)
			{
				throw new BadRequestException("Já existe um usuário cadastrado na base para o email informado!");
			}
			else
			{
				user = UserMapper.mapUserRequestDTOToUser(userRequestDTO);
				user.setActive(true);
				user.setCreationDate(Calendar.getInstance());
				user = userRepository.save(user);
				
				userResponseDTO = UserMapper.mapUserToUserResponseDTO(user);
			}
		}
		else
		{
			throw new BadRequestException("O valor informado para o parâmentro email é inválido.");	
		}

		return userResponseDTO;
	}
	
	@Override
	public UserResponseDTO updateUserAndIncludePublicKey(Long userId, UserPublicKeyDTO userPublicKeyDTO) throws NotFoundException
	{
		UserResponseDTO userResponseDTO = null;
		Optional<User> user = userRepository.findById(userId);		
		if(user.isPresent())
		{
			User userEntity = user.get();
			userEntity.setPublicKey(userPublicKeyDTO.getPublicKey());
			userEntity = userRepository.save(userEntity);
			
			userResponseDTO = UserMapper.mapUserToUserResponseDTO(userEntity);
		}
		else
		{
			throw new NotFoundException("O usuário informado não foi encontrado.");		
		}
	 
		return userResponseDTO;
	}

	@Override
	public UserResponseDTO updateUser(Long userId, UserRequestDTO userRequestDTO) throws NotFoundException, BadRequestException
	{
		UserResponseDTO userResponseDTO = null;
		
		isValidInputs(userRequestDTO.getName(),userRequestDTO.getEmail());
		boolean isValidEmail = isValid(userRequestDTO.getEmail());
		if(isValidEmail)
		{
			Optional<User> user = userRepository.findById(userId);
			if(!user.isPresent())
			{
				throw new NotFoundException("O usuário informado não foi encontrado.");
			}
			else
			{
				User userCheckEmail = userRepository.findByEmail(userRequestDTO.getEmail());
				if(userCheckEmail!=null && !userCheckEmail.getId().equals(userId))
				{				
					throw new BadRequestException("Já existe um outro usuário cadastrado na base para o email informado!");
				}
				else
				{
					User userEntity = user.get();
					userEntity.setName(userRequestDTO.getName());
					userEntity.setEmail(userRequestDTO.getEmail());
					
					userEntity = userRepository.save(userEntity);
					
					userResponseDTO = UserMapper.mapUserToUserResponseDTO(userEntity);
				}
			}
		}
		else
		{
			throw new BadRequestException("O valor informado para o parâmentro email é inválido.");	
		}

		return userResponseDTO;
	}
	
	@Override
	public UserResponseDTO reativateUser(Long userId) throws NotFoundException
	{
		UserResponseDTO userResponseDTO = null;
		Optional<User> user = userRepository.findById(userId);		
		if(user.isPresent())
		{
			User userEntity = user.get();
			userEntity.setActive(true);
			userEntity.setInactivityDate(null);
			userEntity = userRepository.save(userEntity);
			
			userResponseDTO = UserMapper.mapUserToUserResponseDTO(userEntity);
		}
		else
		{
			throw new NotFoundException("O usuário informado não foi encontrado.");		
		}
	 
		return userResponseDTO;
	}

	@Override
	public UserResponseDTO disableUser(Long userId) throws NotFoundException
	{
		UserResponseDTO userResponseDTO = null;		
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent())
		{
			throw new NotFoundException("O usuário informado não foi encontrado.");
		}
		else
		{
			User userEntity = user.get();
			userEntity.setActive(false);
			userEntity.setInactivityDate(Calendar.getInstance());
			
			userEntity = userRepository.save(userEntity);
			
			userResponseDTO = UserMapper.mapUserToUserResponseDTO(userEntity);
		}
		
		return userResponseDTO;
	}	
	

    private boolean isValid(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"; 
 
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
    
    private void isValidInputs(String name, String email) throws BadRequestException 
    { 
		if(name==null || name.isEmpty() || email == null || email.isEmpty())
		{
			throw new BadRequestException("Campo obrigatório não informado na requisição.");	
		}
		else if (name.length() > 100 || email.length() > 100)
		{
			throw new BadRequestException("Tamanho de campo informado na requisição é maior que 100 caracteres. ");	
		}
    } 
 
}
