package br.com.bancointer.desafio.api.dto;

import java.util.Calendar;

public class UniqueDigitResponseDTO
{
	private Long id;
	private Calendar creationDate;
	private Long userId;
	private String nValue;
	private Integer kValue;
	private Integer result;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getnValue() {
		return nValue;
	}
	public void setnValue(String nValue) {
		this.nValue = nValue;
	}
	public Integer getkValue() {
		return kValue;
	}
	public void setkValue(Integer kValue) {
		this.kValue = kValue;
	}
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
}
