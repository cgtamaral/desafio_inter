package br.com.bancointer.desafio.api.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User 
{
	private Long id;
	private String name;
	private String email;
	private String publicKey;
	private List<UniqueDigit> uniqueDigitsList;
 	private Boolean active;
	private Calendar creationDate;
	private Calendar inactivityDate;
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length = 100, nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(length = 100, nullable = false)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(length = 2048, nullable = true)
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	@OneToMany(mappedBy="user")
	public List<UniqueDigit> getUniqueDigitsList() {
		return uniqueDigitsList;
	}
	public void setUniqueDigitsList(List<UniqueDigit> uniqueDigitsList) {
		this.uniqueDigitsList = uniqueDigitsList;
	}
	
	@Column(name = "active", length = 4, nullable = false)
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Column(nullable = false)
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(nullable = true)
	public Calendar getInactivityDate() {
		return inactivityDate;
	}
	public void setInactivityDate(Calendar inactivityDate) {
		this.inactivityDate = inactivityDate;
	}
}
