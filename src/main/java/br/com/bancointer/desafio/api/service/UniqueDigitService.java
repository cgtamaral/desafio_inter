package br.com.bancointer.desafio.api.service;

import br.com.bancointer.desafio.api.dto.UniqueDigitRequestDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

public interface UniqueDigitService {

	UniqueDigitResponseDTO calculateUniqueDigits(UniqueDigitRequestDTO uniqueDigitRequestDTO) throws BadRequestException, NotFoundException;

}
