package br.com.bancointer.desafio.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.util.cryptography.EncriptyUtil;

public class UserMapper 
{
	public static User mapUserRequestDTOToUser(UserRequestDTO userRequestDTO)
	{
		ModelMapper modelMapper = new ModelMapper();
		User user = modelMapper.map(userRequestDTO, User.class);
		
		return user;
	}
	
	public static UserResponseDTO mapUserToUserResponseDTO(User user)
	{
		ModelMapper modelMapper = new ModelMapper();
		UserResponseDTO userResponseDTO = modelMapper.map(user, UserResponseDTO.class);
		if(userResponseDTO.getPublicKey()!=null)
		{
			try {
				String encryptedName = EncriptyUtil.encrypt(userResponseDTO.getName(), userResponseDTO.getPublicKey());
				String encryptedEmail = EncriptyUtil.encrypt(userResponseDTO.getEmail(), userResponseDTO.getPublicKey());
				userResponseDTO.setName(encryptedName);
				userResponseDTO.setEmail(encryptedEmail);
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return userResponseDTO;
	}

	public static List<UserResponseDTO> mapUserListToUserResponseDTOList(List<User> userList)
	{
		ModelMapper modelMapper = new ModelMapper();
		
		List<UserResponseDTO> userResponseDTO =  userList
										  .stream()
										  .map(user -> modelMapper.map(user, UserResponseDTO.class))
										  .collect(Collectors.toList());
		
		return userResponseDTO;
	}
}
