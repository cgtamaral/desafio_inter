package br.com.bancointer.desafio.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.entity.UniqueDigit;

public class UniqueDigitMapper 
{
	public static List<UniqueDigitResponseDTO> mapUniqueDigitListToUniqueDigitResponseDTOList(List<UniqueDigit> uniqueDigitsList)
	{
		ModelMapper modelMapper = new ModelMapper();
		
		List<UniqueDigitResponseDTO> uniqueDigitResponseDTO =  uniqueDigitsList
										  .stream()
										  .map(uniqueDigit -> modelMapper.map(uniqueDigit, UniqueDigitResponseDTO.class))
										  .collect(Collectors.toList());
		
		return uniqueDigitResponseDTO;
	}

	public static UniqueDigitResponseDTO mapUniqueDigitToUniqueDigitResponseDTO(UniqueDigit uniqueDigitEntity) 
	{
		ModelMapper modelMapper = new ModelMapper();
		UniqueDigitResponseDTO uniqueDigitResponseDTO = modelMapper.map(uniqueDigitEntity, UniqueDigitResponseDTO.class);
		
		uniqueDigitResponseDTO.setUserId(uniqueDigitEntity.getUser().getId());
		
		return uniqueDigitResponseDTO;
	}
}
