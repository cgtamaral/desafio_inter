package br.com.bancointer.desafio.api.service.impl;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.bancointer.desafio.api.dto.UniqueDigitRequestDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.entity.UniqueDigit;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.mapper.UniqueDigitMapper;
import br.com.bancointer.desafio.api.repository.UniqueDigitRepository;
import br.com.bancointer.desafio.api.service.UniqueDigitService;
import br.com.bancointer.desafio.api.service.UserService;
import br.com.bancointer.desafio.api.util.cache.DesafioGenericCache;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

@Service
public class UniqueDigitServiceImpl implements UniqueDigitService
{
	private UserService userService;
	private UniqueDigitRepository uniqueDigitRepository;
	private final DesafioGenericCache<String, Object> cache;
	
	public UniqueDigitServiceImpl(UserService p_userService, UniqueDigitRepository p_uniqueDigitRepository,
			DesafioGenericCache<String, Object> p_cache) 
	{
		this.userService = p_userService;
		this.uniqueDigitRepository = p_uniqueDigitRepository;
		this.cache = p_cache;
	}

	@Override
	public UniqueDigitResponseDTO calculateUniqueDigits(UniqueDigitRequestDTO uniqueDigitRequestDTO) throws BadRequestException, NotFoundException
	{	
		validateInputsForGenerateUniqueDigit(uniqueDigitRequestDTO.getnValue(), uniqueDigitRequestDTO.getkValue()); 
		UniqueDigitResponseDTO uniqueDigitResponseDTO = null;
		if(uniqueDigitRequestDTO.getUserId()!= null)
		{
			Optional<User> user  = userService.getUserById(uniqueDigitRequestDTO.getUserId());
			if(user.isPresent())
			{
				String cacheKey = uniqueDigitRequestDTO.getkValue()!=null ? uniqueDigitRequestDTO.getnValue().concat(uniqueDigitRequestDTO.getkValue().toString()) : uniqueDigitRequestDTO.getnValue();
				int uniqueDigit = (int) cache.get(cacheKey).orElseGet(() -> generateUniqueDigit(uniqueDigitRequestDTO.getnValue(), uniqueDigitRequestDTO.getkValue()));
				UniqueDigit uniqueDigitEntity = new UniqueDigit();
				uniqueDigitEntity.setCreationDate(Calendar.getInstance());
				uniqueDigitEntity.setUser(user.get());
				uniqueDigitEntity.setnValue(uniqueDigitRequestDTO.getnValue());
				uniqueDigitEntity.setkValue(uniqueDigitRequestDTO.getkValue());
				uniqueDigitEntity.setResult(uniqueDigit);				
			
				uniqueDigitEntity = uniqueDigitRepository.save(uniqueDigitEntity);
				
				uniqueDigitResponseDTO = UniqueDigitMapper.mapUniqueDigitToUniqueDigitResponseDTO(uniqueDigitEntity);
			}
			else
			{
				throw new NotFoundException("O usuário informado não foi encontrado e o cáculo nao foi realizado");
			}
		}
		else
		{
			String cacheKey = uniqueDigitRequestDTO.getkValue()!=null ? uniqueDigitRequestDTO.getnValue().concat(uniqueDigitRequestDTO.getkValue().toString()) : uniqueDigitRequestDTO.getnValue();
			int uniqueDigit = (int) cache.get(cacheKey).orElseGet(() -> generateUniqueDigit(uniqueDigitRequestDTO.getnValue(), uniqueDigitRequestDTO.getkValue()));
			uniqueDigitResponseDTO = new UniqueDigitResponseDTO();
			uniqueDigitResponseDTO.setnValue(uniqueDigitRequestDTO.getnValue());
			uniqueDigitResponseDTO.setkValue(uniqueDigitRequestDTO.getkValue());
			uniqueDigitResponseDTO.setResult(uniqueDigit);
		}

		return uniqueDigitResponseDTO;
	}
	
	private int generateUniqueDigit(String nValue, Integer kValue) 
	{
		Integer nCopiesValue = kValue!=null ? kValue : 1;
		String currentInteger = String.join("", Collections.nCopies(nCopiesValue, nValue));
		while (currentInteger.length() > 1) 
		{
			BigInteger sum = new BigInteger("0");
			for (char c : currentInteger.toCharArray()) 
			{
				sum = sum.add(new BigInteger(String.valueOf(c)));
			}
			currentInteger = sum.toString();
		}
  
		int uniqueDigit = Integer.parseInt(currentInteger);
		
		String cacheKey = kValue!=null ? nValue.concat(kValue.toString()) : nValue;
		
		this.cache.put(cacheKey, uniqueDigit);
		
		return uniqueDigit;
	}
	
	private void validateInputsForGenerateUniqueDigit(String nValue, Integer kValue) throws BadRequestException
	{
		BigInteger nMaxValue = new BigInteger("10");
		nMaxValue = nMaxValue.pow(1000000);
		BigInteger nValueCheck = null;
		if(nValue!=null && !nValue.isEmpty())
		{
			try
			{
				nValueCheck = new BigInteger(nValue);
			}
			catch (Exception e) 
			{
				throw new BadRequestException("O valor informado para o campo n é invalido.");	
			}
			
			int compareResultMaxNValue = nValueCheck.compareTo(nMaxValue);
		    if (compareResultMaxNValue == 1) 
		    { 
		    	throw new BadRequestException("O valor informado para o campo n é maior que 10ˆ1000000.");	
	        } 
	        else if(nValueCheck.compareTo(new BigInteger("1")) < 0)
	        { 
	        	throw new BadRequestException("O valor informado para o campo n é menor que 1.");	
	        } 
		
			if(kValue!=null)
			{
				Double kMaxValueCheckTemp = Math.pow(10, 5);
				Integer kMaxValueInt = kMaxValueCheckTemp.intValue();
			
				if (kValue > kMaxValueInt) 
			    { 
					throw new BadRequestException("O valor informado para o campo k é maior que 10ˆ5.");	
		        } 
		        else if(kValue < 1)
		        { 
		        	throw new BadRequestException("O valor informado para o campo k é menor que 1.");	
		        } 
			}
		}
		else
		{
			throw new BadRequestException("O campo n não foi informado.");	
		}
	}

}
