package br.com.bancointer.desafio.api.service;

import java.util.List;
import java.util.Optional;

import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.dto.UserPublicKeyDTO;
import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

public interface UserService 
{
	Optional<User> getUserById(Long userId);
	UserResponseDTO getUserByEmail(String email) throws NotFoundException, BadRequestException;
	List<UniqueDigitResponseDTO> getUniqueDigitsByUser(Long userId) throws NotFoundException;
	UserResponseDTO createUser(UserRequestDTO userRequestDTO) throws BadRequestException;
	UserResponseDTO updateUserAndIncludePublicKey(Long userId, UserPublicKeyDTO userPublicKeyDTO) throws NotFoundException;
	UserResponseDTO updateUser(Long userId, UserRequestDTO userRequestDTO) throws NotFoundException, BadRequestException;
	UserResponseDTO reativateUser(Long userId) throws NotFoundException;
	UserResponseDTO disableUser(Long userId) throws NotFoundException;
}
