package br.com.bancointer.desafio.api.dto;

public class UniqueDigitRequestDTO
{
	private Long userId;
	private String nValue;
	private Integer kValue;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getnValue() {
		return nValue;
	}
	public void setnValue(String nValue) {
		this.nValue = nValue;
	}
	public Integer getkValue() {
		return kValue;
	}
	public void setkValue(Integer kValue) {
		this.kValue = kValue;
	}
}
