package br.com.bancointer.desafio.api.dto;

import java.util.Calendar;

public class UserResponseDTO {

	private Long id;
	private String name;
	private String email;
	private String publicKey;
 	private Boolean active;
	private Calendar creationDate;
	private Calendar inactivityDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	public Calendar getInactivityDate() {
		return inactivityDate;
	}
	public void setInactivityDate(Calendar inactivityDate) {
		this.inactivityDate = inactivityDate;
	}
}
