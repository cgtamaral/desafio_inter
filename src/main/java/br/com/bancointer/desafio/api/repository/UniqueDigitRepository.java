package br.com.bancointer.desafio.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.bancointer.desafio.api.entity.UniqueDigit;

@Repository
public interface UniqueDigitRepository extends JpaRepository<UniqueDigit, Long>{

}
