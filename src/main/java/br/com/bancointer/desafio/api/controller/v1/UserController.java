package br.com.bancointer.desafio.api.controller.v1;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.dto.ResponseDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.dto.UserPublicKeyDTO;
import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.service.UserService;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;

@RestController
@RequestMapping("/v1/public")
@CrossOrigin(origins = "*")
@Api(value = "users", description = "Recurso para gerenciamento de usuários. Permite a consulta, criação, edição e inativação de um usuário. "
		+ "Como fucionalidades complementares permite a inclusão ou atualização da chave pública e consulta de dados de digitos únicos para um usuário especifico.", tags={ "users"})
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	private UserService userService;
	
	public UserController(UserService p_userService) 
	{
		this.userService = p_userService;
	}
	
	@ApiOperation(value = "Recupera as informações de cadastro de um usuário considerando o email do mesmo como parâmetro de busca.", nickname = "getUserByEmail", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usuário encontrado com sucesso!"),
    					   @ApiResponse(code = 400, message = "O parâmentro email não foi informado. | O valor informado para o parâmentro email é inválido."),
    					   @ApiResponse(code = 404, message = "Nenhum usuário foi encontrado para o email informado.") })
	@GetMapping(value="/users")
	public ResponseEntity<ResponseDTO<UserResponseDTO>>  getUserByEmail(@ApiParam(value = "Email do usuário a ser consultado.", required = true)  @RequestParam("email") String email)
	{	
		log.info("Buscando dados do usuário pelo email: {} " + email);
		
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
  		try 
  		{
			userResponseDTO = userService.getUserByEmail(email);
		} 
  		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
  		catch (BadRequestException e) 
  		{
			response.setStatus(HttpStatus.BAD_REQUEST.toString());
			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.badRequest().body(response);
		}
  		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Usuário encontrado com sucesso!")));
		
  		return ResponseEntity.ok(response);
	}
	  
	@ApiOperation(value = "Recupera todos os cáculos de digíto único de um usuário específico.", nickname = "getUniqueDigitsByUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Resultados de digitos únicos encontrados com sucesso para o usuário!"),
    					   @ApiResponse(code = 404, message = "Nenhum resultado de digito único para o usuário informado foi encontrado. | Nenhum usuário foi encontrado para o email informado.")})
	@GetMapping(value = "users/{userId}/uniqueDigits")
	public ResponseEntity<ResponseDTO<List<UniqueDigitResponseDTO>>> getUniqueDigitsByUser(@ApiParam(value = "Identificador do usuário", required = true) @PathVariable("userId") Long userId) 
	{ 
		log.info("Buscando informações de calculo de digito unico existentes para o usuário de id: {} " + userId);

		ResponseDTO<List<UniqueDigitResponseDTO>> response = new ResponseDTO<List<UniqueDigitResponseDTO>>();
		List<UniqueDigitResponseDTO> uniqueDigitResponseDTOList = null;
		try
		{
			uniqueDigitResponseDTOList = userService.getUniqueDigitsByUser(userId);			
		}
		catch (NotFoundException e)
		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(uniqueDigitResponseDTOList);
		response.setMessages(Lists.newArrayList(new String("Resultados de digitos únicos encontrados com sucesso para o usuário!")));
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	
	@ApiOperation(value = "Inclui na base um novo usuário. O campo email é utilizado como validação de unicidade do usuário. ", nickname = "createUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Usuário criado com sucesso!"),
    					   @ApiResponse(code = 400, message = "Já existe um usuário cadastrado na base para o email informado! | Campo obrigatório não informado na requisição. | Tamanho de campo informado na requisição é maior que 100 caracteres. | O valor informado para o parâmentro email é inválido.")})
	@PostMapping(value="/users")
	public ResponseEntity<ResponseDTO<UserResponseDTO>> createUser(@ApiParam(value = "Dados do usuário a ser incluído. Todos os campos são obrigatórios.", required = true) @RequestBody UserRequestDTO userRequestDTO)
	{  
		log.info("Incluindo novo usuário: {} " + userRequestDTO.getEmail());
	  
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
		try
		{
			userResponseDTO = userService.createUser(userRequestDTO);
		}
		catch (BadRequestException e) 
  		{
			response.setStatus(HttpStatus.BAD_REQUEST.toString());
			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Usuário criado com sucesso!")));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@ApiOperation(value = "Atualiza na base um usuário já existente. ", nickname = "updateUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usuário atualizado com sucesso!"),
    					   @ApiResponse(code = 400, message = "Já existe um outro usuário cadastrado na base para o email informado! | Campo obrigatório não informado na requisição. | Tamanho de campo informado na requisição é maior que 100 caracteres. | O valor informado para o parâmentro email é inválido."),
    					   @ApiResponse(code = 404, message= "O usuário informado não foi encontrado.")})
	@PutMapping(value="/users/{userId}")
	public ResponseEntity<ResponseDTO<UserResponseDTO>> updateUser(@ApiParam(value = "Identificador do usuário que será atualizado.", required = true)  @PathVariable("userId") Long userId,
			@ApiParam(value = "Dados do usuário a ser atualizado. Todos os campos são obrigatórios.", required = true)  @RequestBody UserRequestDTO userRequestDTO)
	{
		log.info("Atualizando dados do usuário de id: {} " + userId);
		
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
		try
		{
			userResponseDTO = userService.updateUser(userId, userRequestDTO);
		}
		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		catch (BadRequestException e) 
  		{
			response.setStatus(HttpStatus.BAD_REQUEST.toString());
			response.setCode(HttpStatus.BAD_REQUEST.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Usuário atualizado com sucesso!")));
		
		return ResponseEntity.ok(response); 
	}	 
	
	@ApiOperation(value = "Atualiza na base a chave pública para um usuário já existente. ", nickname = "updatePublicKeyForUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Chave pública do usuário atualizada com sucesso!"),
    					   @ApiResponse(code = 404, message = "O usuário informado não foi encontrado.")})
	@PutMapping(value="/users/{userId}/publicKeys")
	public ResponseEntity<ResponseDTO<UserResponseDTO>> updatePublicKeyForUser(@ApiParam(value = "Identificador do usuário que será atualizado.", required = true)  @PathVariable("userId") Long userId,
			@ApiParam(value = "Chave pública do usuário que será incluída ou atualizada.", required = true)  @RequestBody UserPublicKeyDTO userPublicKeyDTO)
	{
		log.info("Atualizando chave pública para o usuário de id: {} " + userId);
			
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
		try
		{
			userResponseDTO = userService.updateUserAndIncludePublicKey(userId, userPublicKeyDTO);
		}
		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Chave pública do usuário atualizada com sucesso!")));
		
		return ResponseEntity.ok(response); 
	}
	
	@ApiOperation(value = "Reativa na base um usuário que esteja inativa. ", nickname = "reactivateUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usuário reativado com sucesso!"),
    					   @ApiResponse(code = 404, message = "O usuário informado não foi encontrado.")})
	@PutMapping(value="/users/{userId}/reactivateUsers")
	public ResponseEntity<ResponseDTO<UserResponseDTO>> reactivateUser(@ApiParam(value = "Identificador do usuário que será reativado.", required = true)  @PathVariable("userId") Long userId)
	{
		log.info("Reativando o usuário de id: {} " + userId);
			
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
		try
		{
			userResponseDTO = userService.reativateUser(userId);
		}
		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Usuário reativado com sucesso!")));
		
		return ResponseEntity.ok(response); 
	}
	
	@ApiOperation(value = "Desativa na base um usuário já existente. ", nickname = "disableUser", notes = "", tags={ "users"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Usuário desativado com sucesso!"),
    					   @ApiResponse(code = 404, message = "O usuário informado não foi encontrado.")})
	@DeleteMapping(value="/users/{userId}")
	public ResponseEntity<ResponseDTO<UserResponseDTO>> disableUser(@ApiParam(value = "Identifcador do usuário a ser desativado.", required = true) @PathVariable("userId") Long userId)
	{
		log.info("Inativando o usuário de id: {} " + userId);
		
		ResponseDTO<UserResponseDTO> response = new ResponseDTO<UserResponseDTO>();
		UserResponseDTO userResponseDTO = null;
		try
		{
			userResponseDTO = userService.disableUser(userId);
		}
		catch (NotFoundException e)
  		{
			response.setStatus(HttpStatus.NOT_FOUND.toString());
			response.setCode(HttpStatus.NOT_FOUND.value());
			response.setMessages(Lists.newArrayList(new String(e.getMessage())));
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		response.setStatus(HttpStatus.OK.toString());
		response.setCode(HttpStatus.OK.value());
		response.setResult(userResponseDTO);
		response.setMessages(Lists.newArrayList(new String("Usuário desativado com sucesso!")));
		
		return ResponseEntity.ok(response); 
	}	
}
