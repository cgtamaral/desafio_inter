package br.com.bancointer.desafio.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/desafio");
		SpringApplication.run(DesafioApplication.class, args);
	}

}
