package br.com.bancointer.desafio.api.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.dto.UniqueDigitRequestDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.entity.UniqueDigit;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.repository.UniqueDigitRepository;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UniqueDigitServiceTest
{
	@Autowired
	private UniqueDigitService uniqueDigitService;
	
	@MockBean
	private UniqueDigitRepository uniqueDigitRepository;
	
	@MockBean
	private UserService userService;
	
	@Test
	public void testCalculateUniqueDigitsErrorUserNotFound() throws Exception {
		
		BDDMockito.given(userService.getUserById(1l)).willReturn(Optional.empty());
		
	    Exception exception = assertThrows(NotFoundException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake());
	    });

	    String expectedMessage = "O usuário informado não foi encontrado e o cáculo nao foi realizado";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	/*
	@Test
	public void testCalculateUniqueDigitsErrorMaxValueForNField() throws Exception {

	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFakeInvalidNValue());
	    });

	    String expectedMessage = "O valor informado para o campo n é maior que 10ˆ1000000.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}*/
	
	@Test
	public void testCalculateUniqueDigitsErrorMinValueNField() throws Exception {
		
	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake2());
	    });

	    String expectedMessage = "O valor informado para o campo n é menor que 1.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorInvalidValueForNField() throws Exception {
		
	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake3());
	    });

	    String expectedMessage = "O valor informado para o campo n é invalido.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMaxValueForKField() throws Exception {

	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFakeInvalidKValue());
	    });

	    String expectedMessage = "O valor informado para o campo k é maior que 10ˆ5.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMinValueKField() throws Exception {
			
	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake4());
	    });

	    String expectedMessage = "O valor informado para o campo k é menor que 1.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorEmptyNField() throws Exception {
		
	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake5());
	    });

	    String expectedMessage = "O campo n não foi informado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCalculateUniqueDigitsSucessEmptyUserIdField() throws Exception {
		
		BDDMockito.given(userService.getUserById(Mockito.anyLong())).willReturn(Optional.empty());
				
		UniqueDigitResponseDTO uniqueDigitResponseDTO =  this.uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFakeNullUser());

		assertNotNull(uniqueDigitResponseDTO);
	}

	@Test
	public void testCalculateUniqueDigitsSucessAndAssociateToUser() throws Exception {
		
		BDDMockito.given(userService.getUserById(Mockito.anyLong())).willReturn(createUserEntityFake());
		BDDMockito.given(this.uniqueDigitRepository.save(Mockito.any(UniqueDigit.class))).willReturn(createUniqueDigitEntityFake().get());
		
		UniqueDigitResponseDTO uniqueDigitResponseDTO =  this.uniqueDigitService.calculateUniqueDigits(createUniqueDigitRequestFake6());

		assertNotNull(uniqueDigitResponseDTO);
	}
	

	private UniqueDigitRequestDTO createUniqueDigitRequestFake() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setUserId(2l);
		uniqueDigitRequestDTO.setnValue("11");
		
		return uniqueDigitRequestDTO;
	}
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFakeNullUser() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("11");
		
		return uniqueDigitRequestDTO;
	}
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFakeInvalidKValue() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("11");
		uniqueDigitRequestDTO.setkValue(100001);
		return uniqueDigitRequestDTO;
	}
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFakeInvalidNValue() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		BigInteger big = new BigInteger("11");
		big = big.pow(100000);
		
		uniqueDigitRequestDTO.setnValue(big.toString());
		uniqueDigitRequestDTO.setkValue(1);
		return uniqueDigitRequestDTO;
	}
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFake2() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("-1");
		
		return uniqueDigitRequestDTO;
	}
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFake3() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("teste");
		
		return uniqueDigitRequestDTO;
	}
	
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFake4() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("11");
		uniqueDigitRequestDTO.setkValue(-1);
		
		return uniqueDigitRequestDTO;
	}
	
	
	private UniqueDigitRequestDTO createUniqueDigitRequestFake5() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setkValue(1);
		
		return uniqueDigitRequestDTO;
	}
	

	private UniqueDigitRequestDTO createUniqueDigitRequestFake6() throws JsonProcessingException
	{
		UniqueDigitRequestDTO uniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		uniqueDigitRequestDTO.setnValue("11");
		uniqueDigitRequestDTO.setUserId(1l);
		
		return uniqueDigitRequestDTO;
	}
	
	private Optional<User> createUserEntityFake()
	{
		User user = new User();
		user.setId(1l);
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());
		
		UniqueDigit uniqueDigit = new UniqueDigit();
		uniqueDigit.setId(1l);
		uniqueDigit.setCreationDate(Calendar.getInstance());
		uniqueDigit.setUser(user);
		uniqueDigit.setnValue("11");
		uniqueDigit.setkValue(null);
		uniqueDigit.setResult(2);
		
		user.setUniqueDigitsList(Lists.newArrayList(uniqueDigit));
		
		return Optional.of(user);
	}
	
	private Optional<UniqueDigit> createUniqueDigitEntityFake()
	{
		User user = new User();
		user.setId(1l);
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());
		
		UniqueDigit uniqueDigit = new UniqueDigit();
		uniqueDigit.setId(1l);
		uniqueDigit.setCreationDate(Calendar.getInstance());
		uniqueDigit.setUser(user);
		uniqueDigit.setnValue("11");
		uniqueDigit.setkValue(null);
		uniqueDigit.setResult(2);
		
		user.setUniqueDigitsList(Lists.newArrayList(uniqueDigit));
		
		return Optional.of(uniqueDigit);
	}

}
