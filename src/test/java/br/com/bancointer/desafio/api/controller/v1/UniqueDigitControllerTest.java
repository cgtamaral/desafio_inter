package br.com.bancointer.desafio.api.controller.v1;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bancointer.desafio.api.dto.UniqueDigitRequestDTO;
import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.service.UniqueDigitService;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UniqueDigitControllerTest 
{
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UniqueDigitService uniqueDigitService;
	
	private static final String POST_UNIQUE_DIGIT_URI = "/v1/public/uniqueDigits";
	
	@Test
	public void testCalculateUniqueDigitsErrorUserNotFound() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new NotFoundException("O usuário informado não foi encontrado e o cáculo nao foi realizado"));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("O usuário informado não foi encontrado e o cáculo nao foi realizado"));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMaxValueForNField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O valor informado para o campo n é maior que 10ˆ1000000."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o campo n é maior que 10ˆ1000000."));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMinValueNField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O valor informado para o campo n é menor que 1."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o campo n é menor que 1."));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorInvalidValueForNField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O valor informado para o campo n é invalido."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o campo n é invalido."));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMaxValueForKField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O valor informado para o campo k é maior que 10ˆ5."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o campo k é maior que 10ˆ5."));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorMinValueKField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O valor informado para o campo k é menor que 1."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o campo k é menor que 1."));
	}
	
	@Test
	public void testCalculateUniqueDigitsErrorEmptyNField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willThrow(new BadRequestException("O campo n não foi informado."));

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O campo n não foi informado."));
	}
	
	@Test
	public void testCalculateUniqueDigitsSucessEmptyUserIdField() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willReturn(createUniqueDigitResponseFake());

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Resultados de digitos únicos calculados com sucesso!"));
	}

	@Test
	public void testCalculateUniqueDigitsSucessAndAssociateToUser() throws Exception {
		
		BDDMockito.given(this.uniqueDigitService.calculateUniqueDigits(Mockito.any(UniqueDigitRequestDTO.class))).willReturn(createUniqueDigitResponseFake2());

		mvc.perform(MockMvcRequestBuilders.post(POST_UNIQUE_DIGIT_URI)
				.content(this.createUniqueDigitRequestPostFake2())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Resultados de digitos únicos calculados e associados ao usuário com sucesso!"));
	}

	

	private String createUniqueDigitRequestPostFake() throws JsonProcessingException
	{
		UniqueDigitRequestDTO UniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		UniqueDigitRequestDTO.setnValue("11");
		
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(UniqueDigitRequestDTO);
	}
	
	private String createUniqueDigitRequestPostFake2() throws JsonProcessingException
	{
		UniqueDigitRequestDTO UniqueDigitRequestDTO = new UniqueDigitRequestDTO();
		UniqueDigitRequestDTO.setnValue("11");
		UniqueDigitRequestDTO.setUserId(1l);
		
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(UniqueDigitRequestDTO);
	}
	
	private UniqueDigitResponseDTO createUniqueDigitResponseFake()
	{
		UniqueDigitResponseDTO uniqueDigitResponseDTO = new UniqueDigitResponseDTO();
		uniqueDigitResponseDTO.setId(1l);
		uniqueDigitResponseDTO.setnValue("11");
		uniqueDigitResponseDTO.setResult(2);
		
		return uniqueDigitResponseDTO;	
	}
	
	private UniqueDigitResponseDTO createUniqueDigitResponseFake2()
	{
		UniqueDigitResponseDTO uniqueDigitResponseDTO = new UniqueDigitResponseDTO();
		uniqueDigitResponseDTO.setId(1l);
		uniqueDigitResponseDTO.setUserId(1l);
		uniqueDigitResponseDTO.setnValue("11");
		uniqueDigitResponseDTO.setResult(2);
		
		return uniqueDigitResponseDTO;	
	}
}	

