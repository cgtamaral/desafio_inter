package br.com.bancointer.desafio.api.repository;

import static org.junit.Assert.assertNotNull;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.entity.UniqueDigit;
import br.com.bancointer.desafio.api.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class UniqueDigitRepositoryTest {
	
	@Autowired
	private UniqueDigitRepository uniqueDigitRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Test
	public void testSaveUniqueDigitRepository() throws Exception 
	{
		User user = userRepository.save(createUserEntityFake());
		UniqueDigit uniqueDigit = uniqueDigitRepository.save(createUniqueDigitFake(user));
		
		assertNotNull(uniqueDigit);
		assertNotNull(uniqueDigit.getId());
	}
	
	private User createUserEntityFake()
	{
		User user = new User();
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());

		
		return user;
	}
	
	private UniqueDigit createUniqueDigitFake(User user)
	{	
		UniqueDigit uniqueDigit = new UniqueDigit();
		uniqueDigit.setCreationDate(Calendar.getInstance());
		uniqueDigit.setUser(user);
		uniqueDigit.setnValue("11");
		uniqueDigit.setkValue(null);
		uniqueDigit.setResult(2);
		
		user.setUniqueDigitsList(Lists.newArrayList(uniqueDigit));
		
		return uniqueDigit;
	}
}
