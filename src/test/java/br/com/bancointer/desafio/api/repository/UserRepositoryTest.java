package br.com.bancointer.desafio.api.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.bancointer.desafio.api.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void testSaveUser() throws Exception 
	{
		User user = userRepository.save(createUserEntityFake());
		assertNotNull(user);
		assertNotNull(user.getId());
	}
	
	
	@Test
	public void testFindByEmail() throws Exception 
	{
		userRepository.save(createUserEntityFake2());
		
		User userConsulta = userRepository.findByEmail("usuariofake2@gmail.com");
		
		assertNotNull(userConsulta);
		assertEquals("usuariofake2@gmail.com", userConsulta.getEmail());
	}
	
	@Test
	public void testFindById() throws Exception 
	{
		User user = userRepository.save(createUserEntityFake3());
		
		Optional<User> userConsulta = userRepository.findById(user.getId());
		
		assertNotNull(userConsulta.get());
		assertEquals(user.getId(), userConsulta.get().getId());
	}

	private User createUserEntityFake()
	{
		User user = new User();
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());

		
		return user;
	}
	
	private User createUserEntityFake2()
	{
		User user = new User();
		user.setName("Usuário Fake 2");
		user.setEmail("usuariofake2@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());

		
		return user;
	}
	
	private User createUserEntityFake3()
	{
		User user = new User();
		user.setName("Usuário Fake 3");
		user.setEmail("usuariofake3@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());

		
		return user;
	}
}
