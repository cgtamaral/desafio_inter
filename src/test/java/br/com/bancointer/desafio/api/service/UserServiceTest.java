package br.com.bancointer.desafio.api.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.dto.UserPublicKeyDTO;
import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.entity.UniqueDigit;
import br.com.bancointer.desafio.api.entity.User;
import br.com.bancointer.desafio.api.repository.UserRepository;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserServiceTest 
{
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepository userRepository;
	
	@Test
	public void testGetUserByEmailErrorInvalidEmail() throws Exception 
	{    
	    Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.getUserByEmail("testeInvalido");
	    });

	    String expectedMessage = "O valor informado para o parâmentro email é inválido.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	
	@Test
	public void testGetUserByEmailErrorUserNotFound() throws Exception {

		BDDMockito.given(this.userRepository.findByEmail(Mockito.anyString())).willReturn(null);
		
	 	Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.getUserByEmail("usuarioteste1@gmail.com");
	    });

	    String expectedMessage = "Nenhum usuário foi encontrado para o email informado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testGetUserByEmailSucess() throws Exception 
	{
		BDDMockito.given(this.userRepository.findByEmail(Mockito.anyString())).willReturn(createUserEntityFake1());

		UserResponseDTO userResponseDTO =  this.userService.getUserByEmail("usuariofake1@gmail.com");

		assertNotNull(userResponseDTO);
	}
	
	@Test
	public void  testGetUniqueDigitsByUserErrorResultsNotFound() throws Exception 
	{	
		BDDMockito.given(this.userRepository.findById(1l)).willReturn(createUserEntityFake());

		Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.getUniqueDigitsByUser(1l);
	    });

	    String expectedMessage = "Nenhum resultado de digito único para o usuário informado foi encontrado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));

	}
	
	@Test
	public void  testGetUniqueDigitsByUserErrorUserNotFound() throws Exception {
	 	
		BDDMockito.given(this.userRepository.findById(Mockito.anyLong())).willReturn(Optional.empty());
		
		Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.getUniqueDigitsByUser(1l);
	    });

	    String expectedMessage = "O usuário informado não foi encontrado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void  testGetUniqueDigitsByUserSucess() throws Exception {
		 
		BDDMockito.given(this.userRepository.findById(1l)).willReturn(createUserEntityFake2());

		List<UniqueDigitResponseDTO> uniqueDigitResponseDTO =  this.userService.getUniqueDigitsByUser(1l);

		assertNotNull(uniqueDigitResponseDTO);
	}
	
	@Test
	public void testCreateUserErrorUserAlreadyExist() throws Exception {

		BDDMockito.given(this.userRepository.findByEmail(Mockito.anyString())).willReturn(createUserEntityFake1());

		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.createUser(createUserRequestFake());
	    });

	    String expectedMessage = "Já existe um usuário cadastrado na base para o email informado!";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	public void testCreateUserErrorIncompleteRequest() throws Exception {
		
		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.createUser(createUserRequestFake3());
	    });

	    String expectedMessage = "Campo obrigatório não informado na requisição.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCreateUserErrorInvalidFieldSize() throws Exception {
		
		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.createUser(createUserRequestFake1());
	    });

	    String expectedMessage = "Tamanho de campo informado na requisição é maior que 100 caracteres.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCreateUserErrorInvalidEmail() throws Exception {
		
		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.createUser(createUserRequestFake2());
	    });

	    String expectedMessage = "O valor informado para o parâmentro email é inválido.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testCreateUserSucess() throws Exception {
		
		BDDMockito.given(this.userRepository.save(Mockito.any(User.class))).willReturn(createUserEntityFake().get());
		
		UserResponseDTO userResponseDTO = this.userService.createUser(createUserRequestFake());
		
		assertNotNull(userResponseDTO);
	}
	
	@Test
	public void testUpdateUserErrorIncompleteRequest() throws Exception {

		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.updateUser(1l,createUserRequestFake3());
	    });

	    String expectedMessage = "Campo obrigatório não informado na requisição.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testUpdateUserErrorInvalidFieldSize() throws Exception 
	{	    
		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.updateUser(1l, createUserRequestFake1());
	    });

	    String expectedMessage = "Tamanho de campo informado na requisição é maior que 100 caracteres.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testUpdateUserErrorInvalidEmail() throws Exception {
		
		Exception exception = assertThrows(BadRequestException.class, () -> {
	    	   this.userService.updateUser(1l,createUserRequestFake2());
	    });

	    String expectedMessage = "O valor informado para o parâmentro email é inválido.";
	    String actualMessage = exception.getMessage();

	    assertEquals(actualMessage,expectedMessage);
	}
	
	@Test
	public void testUpdateUserErrorUserNotFound() throws Exception {
		
		BDDMockito.given(this.userRepository.findById(Mockito.anyLong())).willReturn(Optional.empty());
		
		Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.updateUser(Mockito.anyLong(),createUserRequestFake());
	    });

	    String expectedMessage = "O usuário informado não foi encontrado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testUpdateUserSucess() throws Exception {
		
		BDDMockito.given(this.userRepository.findById(Mockito.anyLong())).willReturn(createUserEntityFake());
		BDDMockito.given(this.userRepository.save(Mockito.any(User.class))).willReturn(createUserEntityFake().get());
		
		UserResponseDTO userResponseDTO = this.userService.updateUser(Mockito.anyLong(),createUserRequestFake());
		
		assertNotNull(userResponseDTO);
	}
	
	@Test
	public void testUpdatePublicKeyForUserErrorUserNotFound() throws Exception {
		
		BDDMockito.given(this.userRepository.findById(1l)).willReturn(createUserEntityFake());
		
		Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.updateUserAndIncludePublicKey(2l, Mockito.any(UserPublicKeyDTO.class));
	    });
	
	    String expectedMessage = "O usuário informado não foi encontrado.";
	    String actualMessage = exception.getMessage();
	
	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testUpdatePublicKeyForUserSucess() throws Exception 
	{	
		BDDMockito.given(this.userRepository.findById(Mockito.anyLong())).willReturn(createUserEntityFake());
		BDDMockito.given(this.userRepository.save(Mockito.any(User.class))).willReturn(createUserEntityFake().get());
		
		UserResponseDTO userResponseDTO = this.userService.updateUserAndIncludePublicKey(1l,createUserPublicKeyFake());
		
		assertNotNull(userResponseDTO);
	}
	
	@Test
	public void testDisableUserErrorUserNotFound() throws Exception {

		BDDMockito.given(this.userRepository.findById(1l)).willReturn(createUserEntityFake());
		
		Exception exception = assertThrows(NotFoundException.class, () -> {
	    	   this.userService.disableUser(2l);
	    });

	    String expectedMessage = "O usuário informado não foi encontrado.";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testDisableUserSucess() throws Exception 
	{
		BDDMockito.given(this.userRepository.findById(Mockito.anyLong())).willReturn(createUserEntityFake());
		BDDMockito.given(this.userRepository.save(Mockito.any(User.class))).willReturn(createUserEntityFake().get());
		
		UserResponseDTO userResponseDTO = this.userService.disableUser(Mockito.anyLong());
		
		assertNotNull(userResponseDTO);
	}
	
	private Optional<User> createUserEntityFake()
	{
		User user = new User();
		user.setId(1l);
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());
		
		return Optional.of(user);
	}
	
	private User createUserEntityFake1()
	{
		User user = new User();
		user.setId(1l);
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());
		
		return user;
	}
	
	private Optional<User> createUserEntityFake2()
	{
		User user = new User();
		user.setId(1l);
		user.setName("Usuário Fake 1");
		user.setEmail("usuariofake1@gmail.com");
		user.setActive(true);
		user.setCreationDate(Calendar.getInstance());
		
		UniqueDigit uniqueDigit = new UniqueDigit();
		uniqueDigit.setId(1l);
		uniqueDigit.setCreationDate(Calendar.getInstance());
		uniqueDigit.setUser(user);
		uniqueDigit.setnValue("11");
		uniqueDigit.setkValue(null);
		uniqueDigit.setResult(2);
		
		user.setUniqueDigitsList(Lists.newArrayList(uniqueDigit));
		
		return Optional.of(user);
	}
	
	private UserRequestDTO createUserRequestFake()
	{
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setEmail("usuariofake1@gmail.com");
		userRequestDTO.setName("Usuário Fake 1");
		
		return userRequestDTO;
	}
	
	private UserRequestDTO createUserRequestFake1()
	{
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setEmail("usuariofake1@gmail.com");
		userRequestDTO.setName("Para retornar a Portugal, passageiros precisam apresentar um teste negativo de Covid-19 realizado no máximo 72 horas antes da decolagem e fazer quarentena por 14 dias ao chegar, afirmou o comunicado, com medidas que já estavam em vigor.");
		
		return userRequestDTO;
	}

	private UserRequestDTO createUserRequestFake2()
	{
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setEmail("usuariofake1");
		userRequestDTO.setName("Usuário Fake 1");
		
		return userRequestDTO;
	}
	

	private UserRequestDTO createUserRequestFake3()
	{
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setName("Usuário Fake 1");
		
		return userRequestDTO;
	}
	
	private UserPublicKeyDTO createUserPublicKeyFake() throws JsonProcessingException {
		
		UserPublicKeyDTO userRequestDTO = new UserPublicKeyDTO();
		userRequestDTO.setPublicKey("12345678910");
		
		return userRequestDTO;
	}
}
