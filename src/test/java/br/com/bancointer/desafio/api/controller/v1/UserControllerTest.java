package br.com.bancointer.desafio.api.controller.v1;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import br.com.bancointer.desafio.api.dto.UniqueDigitResponseDTO;
import br.com.bancointer.desafio.api.dto.UserPublicKeyDTO;
import br.com.bancointer.desafio.api.dto.UserRequestDTO;
import br.com.bancointer.desafio.api.dto.UserResponseDTO;
import br.com.bancointer.desafio.api.service.UserService;
import br.com.bancointer.desafio.api.util.exception.BadRequestException;
import javassist.NotFoundException;;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {


	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UserService userService;
	
	private static final String GET_POST_UPDATE_DELETE_URI = "/v1/public/users";
	private static final String GET_UNIQUE_DIGITS_FOR_USER_URI = "/v1/public/users/{userId}/uniqueDigits";
	private static final String PUT_PUBLIC_KEY_URI = "/v1/public/users/{userId}/publicKeys";
	



	@Test
	public void testGetUserByEmailErrorInvalidEmail() throws Exception {
		BDDMockito.given(this.userService.getUserByEmail("testeInvalido")).willThrow(new BadRequestException("O valor informado para o parâmentro email é inválido."));

		mvc.perform(MockMvcRequestBuilders.get(GET_POST_UPDATE_DELETE_URI + "?email=testeInvalido").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o parâmentro email é inválido."));
	}
	
	@Test
	public void testGetUserByEmailErrorUserNotFound() throws Exception {
		BDDMockito.given(this.userService.getUserByEmail("usuarioteste1@gmail.com")).willThrow(new NotFoundException("Nenhum usuário foi encontrado para o email informado."));

		mvc.perform(MockMvcRequestBuilders.get(GET_POST_UPDATE_DELETE_URI + "?email=usuarioteste1@gmail.com").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("Nenhum usuário foi encontrado para o email informado."));
	}
	
	@Test
	public void testGetUserByEmailSucess() throws Exception {
		BDDMockito.given(this.userService.getUserByEmail("usuarioteste1@gmail.com")).willReturn(createFakeUser1());

		mvc.perform(MockMvcRequestBuilders.get(GET_POST_UPDATE_DELETE_URI + "?email=usuarioteste1@gmail.com").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Usuário encontrado com sucesso!"));
	}
	
	@Test
	public void  testGetUniqueDigitsByUserErrorResultsNotFound() throws Exception 
	{	
		BDDMockito.given(this.userService.getUniqueDigitsByUser(1l)).willThrow(new NotFoundException("Nenhum resultado de digito único para o usuário informado foi encontrado."));

		mvc.perform(MockMvcRequestBuilders.get(GET_UNIQUE_DIGITS_FOR_USER_URI.replace("{userId}", "1")).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("Nenhum resultado de digito único para o usuário informado foi encontrado."));
	}
	
	@Test
	public void  testGetUniqueDigitsByUserErrorUserNotFound() throws Exception {
	 
		
		BDDMockito.given(this.userService.getUniqueDigitsByUser(2l)).willThrow(new NotFoundException("Nenhum usuário foi encontrado para o email informado."));

		mvc.perform(MockMvcRequestBuilders.get(GET_UNIQUE_DIGITS_FOR_USER_URI.replace("{userId}", "2")).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("Nenhum usuário foi encontrado para o email informado."));
	}
	
	@Test
	public void  testGetUniqueDigitsByUserSucess() throws Exception {
		 
		BDDMockito.given(this.userService.getUniqueDigitsByUser(1l)).willReturn(createFakeUniqueDigitsResults());

		mvc.perform(MockMvcRequestBuilders.get(GET_UNIQUE_DIGITS_FOR_USER_URI.replace("{userId}", "1")).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("Resultados de digitos únicos encontrados com sucesso para o usuário!"));
	}
	
	@Test
	public void testCreateUserErrorUserAlreadyExist() throws Exception {

	    Mockito.doThrow(new BadRequestException("Já existe um usuário cadastrado na base para o email informado!")).when(userService).createUser(Mockito.any(UserRequestDTO.class));	    
	    
		mvc.perform(MockMvcRequestBuilders.post(GET_POST_UPDATE_DELETE_URI)
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("Já existe um usuário cadastrado na base para o email informado!"));
	}

	@Test
	public void testCreateUserErrorIncompleteRequest() throws Exception {
		
	    Mockito.doThrow(new BadRequestException("Campo obrigatório não informado na requisição.")).when(userService).createUser(Mockito.any(UserRequestDTO.class));	    
	    
		mvc.perform(MockMvcRequestBuilders.post(GET_POST_UPDATE_DELETE_URI)
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("Campo obrigatório não informado na requisição."));
	}
	
	@Test
	public void testCreateUserErrorInvalidFieldSize() throws Exception {
			
	    Mockito.doThrow(new BadRequestException("Tamanho de campo informado na requisição é maior que 100 caracteres.")).when(userService).createUser(Mockito.any(UserRequestDTO.class));	    
	    
		mvc.perform(MockMvcRequestBuilders.post(GET_POST_UPDATE_DELETE_URI)
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("Tamanho de campo informado na requisição é maior que 100 caracteres."));
	}
	
	@Test
	public void testCreateUserErrorInvalidEmail() throws Exception {
		
	    Mockito.doThrow(new BadRequestException("O valor informado para o parâmentro email é inválido.")).when(userService).createUser(Mockito.any(UserRequestDTO.class));	    
	    
		mvc.perform(MockMvcRequestBuilders.post(GET_POST_UPDATE_DELETE_URI)
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o parâmentro email é inválido."));
	}
	
	@Test
	public void testCreateUserSucess() throws Exception {
		
		BDDMockito.given(this.userService.createUser(createUserRequestFake())).willReturn(createFakeUser3());
		
		mvc.perform(MockMvcRequestBuilders.post(GET_POST_UPDATE_DELETE_URI)
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.messages").value("Usuário criado com sucesso!"));
	}
	
	@Test
	public void testUpdateUserErrorIncompleteRequest() throws Exception {

	    Mockito.doThrow(new BadRequestException("Campo obrigatório não informado na requisição.")).when(userService).updateUser(Mockito.anyLong(),Mockito.any(UserRequestDTO.class));	    
    	
		mvc.perform(MockMvcRequestBuilders.put(GET_POST_UPDATE_DELETE_URI + "/1")
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("Campo obrigatório não informado na requisição."));
	}
	
	@Test
	public void testUpdateUserErrorInvalidFieldSize() throws Exception {
	
	    Mockito.doThrow(new BadRequestException("Tamanho de campo informado na requisição é maior que 100 caracteres.")).when(userService).updateUser(Mockito.anyLong(), Mockito.any(UserRequestDTO.class));	    
	    
		mvc.perform(MockMvcRequestBuilders.put(GET_POST_UPDATE_DELETE_URI + "/1")
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("Tamanho de campo informado na requisição é maior que 100 caracteres."));
	}
	
	@Test
	public void testUpdateUserErrorInvalidEmail() throws Exception {
		
	    Mockito.doThrow(new BadRequestException("O valor informado para o parâmentro email é inválido.")).when(userService).updateUser(Mockito.anyLong(),Mockito.any(UserRequestDTO.class));	    
	    	    
		mvc.perform(MockMvcRequestBuilders.put(GET_POST_UPDATE_DELETE_URI + "/1")
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.messages").value("O valor informado para o parâmentro email é inválido."));
	}
	
	@Test
	public void testUpdateUserErrorUserNotFound() throws Exception {
		
	    Mockito.doThrow(new NotFoundException("O usuário informado não foi encontrado.")).when(userService).updateUser(Mockito.anyLong(), Mockito.any(UserRequestDTO.class));
	    
		mvc.perform(MockMvcRequestBuilders.put(GET_POST_UPDATE_DELETE_URI + "/3")
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("O usuário informado não foi encontrado."));
	}
	
	@Test
	public void testUpdateUserSucess() throws Exception {
		
		BDDMockito.given(this.userService.updateUser(Mockito.anyLong(),Mockito.any(UserRequestDTO.class))).willReturn(createFakeUser2());

		mvc.perform(MockMvcRequestBuilders.put(GET_POST_UPDATE_DELETE_URI + "/2")
				.content(this.createUserRequestPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Usuário atualizado com sucesso!"));
	}
	
	@Test
	public void testUpdatePublicKeyForUserErrorUserNotFound() throws Exception {
		
	    Mockito.doThrow(new NotFoundException("O usuário informado não foi encontrado.")).when(userService).updateUserAndIncludePublicKey(Mockito.anyLong(), Mockito.any(UserPublicKeyDTO.class));
	    
		mvc.perform(MockMvcRequestBuilders.put(PUT_PUBLIC_KEY_URI.replace("{userId}", "1"))
				.content(this.createUserPublicKeyPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("O usuário informado não foi encontrado."));
	}
	
	@Test
	public void testUpdatePublicKeyForUserSucess() throws Exception 
	{	
		BDDMockito.given(this.userService.updateUserAndIncludePublicKey(Mockito.anyLong(), Mockito.any(UserPublicKeyDTO.class))).willReturn(createFakeUser1());
		   
		mvc.perform(MockMvcRequestBuilders.put(PUT_PUBLIC_KEY_URI.replace("{userId}", "1"))
				.content(this.createUserPublicKeyPostFake())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Chave pública do usuário atualizada com sucesso!"));
	}
	
	@Test
	public void testDisableUserErrorUserNotFound() throws Exception {
		
	    Mockito.doThrow(new NotFoundException("O usuário informado não foi encontrado.")).when(userService).disableUser(Mockito.anyLong());	    
	    
		mvc.perform(MockMvcRequestBuilders.delete(GET_POST_UPDATE_DELETE_URI + "/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.messages").value("O usuário informado não foi encontrado."));
	}
	
	@Test
	public void testDisableUserSucess() throws Exception {
		BDDMockito.given(this.userService.disableUser(Mockito.anyLong())).willReturn(createFakeUser3());

		mvc.perform(MockMvcRequestBuilders.delete(GET_POST_UPDATE_DELETE_URI + "/1")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.messages").value("Usuário desativado com sucesso!"));
	}
	
	private UserResponseDTO createFakeUser1()
	{
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setId(1l);
		userResponseDTO.setName("Usuário de Teste 1");
		userResponseDTO.setEmail("usuarioteste1@gmail.com");
		userResponseDTO.setPublicKey("chave_publica_fake");
		userResponseDTO.setActive(true);
		userResponseDTO.setCreationDate(Calendar.getInstance());
		userResponseDTO.setInactivityDate(null);
		
		return userResponseDTO;
	}
	
	private UserResponseDTO createFakeUser2()
	{
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setId(2l);
		userResponseDTO.setName("Usuário de Teste 2");
		userResponseDTO.setEmail("usuarioteste2@gmail.com");
		userResponseDTO.setPublicKey(null);
		userResponseDTO.setActive(true);
		userResponseDTO.setCreationDate(Calendar.getInstance());
		userResponseDTO.setInactivityDate(null);
		
		return userResponseDTO;
	}
	
	private UserResponseDTO createFakeUser3()
	{
		UserResponseDTO userResponseDTO = new UserResponseDTO();
		userResponseDTO.setId(2l);
		userResponseDTO.setName("Usuário de Teste 3");
		userResponseDTO.setEmail("usuarioteste3@gmail.com");
		userResponseDTO.setPublicKey(null);
		userResponseDTO.setActive(false);
		userResponseDTO.setCreationDate(Calendar.getInstance());
		userResponseDTO.setInactivityDate(null);
		
		return userResponseDTO;
	}
	
	private List<UniqueDigitResponseDTO> createFakeUniqueDigitsResults()
	{
		UniqueDigitResponseDTO uniqueDigitResponseDTO = new UniqueDigitResponseDTO();
		uniqueDigitResponseDTO.setId(1l);
		uniqueDigitResponseDTO.setCreationDate(Calendar.getInstance());
		uniqueDigitResponseDTO.setUserId(1l);
		uniqueDigitResponseDTO.setnValue("11");
		uniqueDigitResponseDTO.setkValue(null);
		uniqueDigitResponseDTO.setResult(2);	
		
		return Lists.newArrayList(uniqueDigitResponseDTO);
	}
	
	private String createUserRequestPostFake() throws JsonProcessingException {
		
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		userRequestDTO.setEmail("usuarioteste1@gmail.com");
		userRequestDTO.setName("Usuário de Teste 1");
		
		ObjectMapper mapper = new ObjectMapper();
	
		return mapper.writeValueAsString(userRequestDTO);
	}
	
	private String createUserPublicKeyPostFake() throws JsonProcessingException {
		
		UserPublicKeyDTO userRequestDTO = new UserPublicKeyDTO();
		userRequestDTO.setPublicKey("12345678910");
		
		ObjectMapper mapper = new ObjectMapper();
	
		return mapper.writeValueAsString(userRequestDTO);
	}
	
	private UserRequestDTO createUserRequestFake() {
		
		UserRequestDTO userRequestDTO = new UserRequestDTO();
		
		return userRequestDTO;
	}
}
